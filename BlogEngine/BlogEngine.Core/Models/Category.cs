﻿using BlogEngine.Shared.Validations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlogEngine.Core.Models
{
    public class Category : BaseEntity
    {
        [Required(ErrorMessage = "{0} is required")]
        [StringLength(50, ErrorMessage = "{0} should not be more than 100 Characters")]
        [FirstLetterUppercase]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        public byte[] GeneralCover { get; set; }

        public List<BlogCategory> BlogCategories { get; set; } = new List<BlogCategory>();
    }
}