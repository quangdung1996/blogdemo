﻿namespace BlogEngine.Core.Models
{
    public class SubComment : BaseComment
    {
        public int MainCommentID { get; set; }
    }
}