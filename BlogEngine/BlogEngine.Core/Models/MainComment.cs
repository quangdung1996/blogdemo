﻿using System.Collections.Generic;

namespace BlogEngine.Core.Models
{
    public class MainComment : BaseComment
    {
        public List<SubComment> SubComments { get; set; } = new List<SubComment>();
    }
}