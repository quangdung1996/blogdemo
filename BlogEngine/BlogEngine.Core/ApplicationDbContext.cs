﻿using BlogEngine.Core.Common;
using BlogEngine.Core.Models;
using BlogEngine.Core.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.Core
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, IdentityRole<int>, int>
    {
        private readonly ICurrentUserProvider _currentUserProvider;
        public DbSet<BlogRating> BlogRatings { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, ICurrentUserProvider currentUserProvider) : base(options)
        {
            _currentUserProvider = currentUserProvider;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BlogCategory>()
                .HasKey(b => new { b.BlogID, b.CategoryID });

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            FillBaseEntityFields().GetAwaiter().GetResult();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await FillBaseEntityFields();
            return await base.SaveChangesAsync(cancellationToken);
        }

        private async Task FillBaseEntityFields()
        {
            var user = await _currentUserProvider.GetCurrentUserAsync();
            string userFullName = user is null ? "Anonymous" : user.FullName;

            var now = DateTime.Now;

            var entityEntries = ChangeTracker
                .Entries<BaseEntity>()
                .ToList();

            entityEntries.ForEach(e =>
            {
                var entity = e.Entity;

                entity.LastUpdateDate = now;
                entity.LastUpdateBy = userFullName;

                switch (e.State)
                {
                    case EntityState.Added:
                        entity.DateCreated = now;
                        entity.CreatedBy = userFullName;
                        break;

                    case EntityState.Modified:

                        #region DateCreated and CreatedBy value should not be changed

                        entity.DateCreated = e.GetDatabaseValues().GetValue<DateTime>(BaseEntityFields.DateCreated);
                        entity.CreatedBy = e.GetDatabaseValues().GetValue<string>(BaseEntityFields.CreatedBy);

                        #endregion DateCreated and CreatedBy value should not be changed

                        break;
                }
            });
        }
    }
}