﻿using BlogEngine.Core.Models;
using System.Threading.Tasks;

namespace BlogEngine.Core.Services
{
    public interface ICurrentUserProvider
    {
        Task<ApplicationUser> GetCurrentUserAsync();

        Task<int> GetCurrentUserIDAsync() => Task.FromResult(GetCurrentUserAsync().GetAwaiter().GetResult().Id);

        Task<bool> IsCurrentUserAdmin();
    }
}