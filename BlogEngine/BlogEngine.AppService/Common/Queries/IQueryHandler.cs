﻿using BlogEngine.Shared.Contracts;
using MediatR;

namespace BlogEngine.AppService.Common.Queries
{
    public interface IQueryHandler<in TQuery, TResult> :
        IRequestHandler<TQuery, TResult>
        where TQuery : IQuery<TResult>
    {
    }
}