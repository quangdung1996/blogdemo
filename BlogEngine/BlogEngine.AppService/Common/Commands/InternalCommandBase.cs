﻿using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.AppService.Common.Commands
{
    public abstract class InternalCommandBase : ICommand
    {
        protected InternalCommandBase(ReturnResponse<UserTokenDTO> returnResponse)
        {
            ReturnResponse = returnResponse;
        }

        public ReturnResponse<UserTokenDTO> ReturnResponse { get; }
    }

    public abstract class InternalCommandBase<TResult> : ICommand<TResult>
    {
        protected InternalCommandBase()
        {
            ReturnResponse = new ReturnResponse<UserTokenDTO>();
        }

        protected InternalCommandBase(ReturnResponse<UserTokenDTO> returnResponse)
        {
            ReturnResponse = returnResponse;
        }

        public ReturnResponse<UserTokenDTO> ReturnResponse { get; }
    }
}