﻿using Autofac;
using BlogEngine.Shared.Contracts;
using MediatR;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Common.Processing
{
    internal static class CommandsExecutor
    {
        internal static async Task ExecuteAsync(ICommand command)
        {
            using (var scope = AdministrationCompositionRoot.BeginLifetimeScope())
            {
                var mediator = scope.Resolve<IMediator>();
                await mediator.Send(command);
            }
        }

        internal static async Task<TResult> ExecuteAsync<TResult>(ICommand<TResult> command)
        {
            using (var scope = AdministrationCompositionRoot.BeginLifetimeScope())
            {
                var mediator = scope.Resolve<IMediator>();
                return await mediator.Send(command);
            }
        }
    }
}