﻿using BlogEngine.AppService.Common.Commands;
using System.Reflection;

namespace BlogEngine.AppService.Common
{
    internal static class Assemblies
    {
        public static readonly Assembly Application = typeof(InternalCommandBase<>).Assembly;
        //public static readonly Assembly ApplicationUpdate = typeof(InternalCommandUpdate<>).Assembly;
    }
}