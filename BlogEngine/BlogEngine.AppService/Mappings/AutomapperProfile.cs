﻿using AutoMapper;
using BlogEngine.Core.Models;
using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.AppService.Mappings
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            #region Identity

            CreateMap<UserLoginDTO, ApplicationUser>()
                .ForMember(au => au.Email, opt => opt.MapFrom(ul => ul.EmailAddress))
                .ForMember(au => au.UserName, opt => opt.MapFrom(ul => ul.EmailAddress));

            CreateMap<UserRegisterDTO, ApplicationUser>()
                .ForMember(au => au.Email, opt => opt.MapFrom(ur => ur.EmailAddress))
                .ForMember(au => au.UserName, opt => opt.MapFrom(ur => ur.EmailAddress));

            CreateMap<ApplicationUser, UserInfoDetailDTO>()
                .ForMember(ui => ui.EmailAddress, opt => opt.MapFrom(au => au.Email))
                .ForMember(ui => ui.ID, opt => opt.MapFrom(au => au.Id));

            CreateMap<ApplicationUser, UserProfileDTO>()
                //.ForMember(up => up.BlogDTOs, opt => opt.MapFrom(au => au.Blogs))
                .ForMember(up => up.ID, opt => opt.MapFrom(au => au.Id))
                .ForMember(up => up.EmailAddress, opt => opt.MapFrom(au => au.Email));

            #endregion Identity
        }
    }
}