﻿using BlogEngine.AppService.Common.Commands;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Models;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Commands
{
    internal sealed class CreateBlogRatingCommnandHandler : ICommandHandler<CreateBlogRatingCommnand, ReturnResponse>
    {
        private readonly IBlogRatingService _service;

        public CreateBlogRatingCommnandHandler(IBlogRatingService service)
        {
            _service = service;
        }

        public async Task<ReturnResponse> Handle(CreateBlogRatingCommnand request, CancellationToken cancellationToken)
        {
            return new ReturnResponse
            {
                Succeeded = await _service.InsertAsync(request.BlogRatingDTO)
            };
        }
    }
}