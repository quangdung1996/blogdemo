﻿using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.AppService.Commands
{
    public class CreateBlogRatingCommnand : CommandBase<ReturnResponse>
    {
        public CreateBlogRatingCommnand(BlogRatingDTO blogRatingDTO)
        {
            BlogRatingDTO = blogRatingDTO;
        }

        internal BlogRatingDTO BlogRatingDTO { get; }
    }
}