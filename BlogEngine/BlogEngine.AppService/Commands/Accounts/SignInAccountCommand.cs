﻿using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.AppService.Commands
{
    public class SignInAccountCommand : CommandBase<ReturnResponse<UserTokenDTO>>
    {
        public SignInAccountCommand(UserLoginDTO userLoginDTO)
        {
            UserLoginDTO = userLoginDTO;
        }

        internal UserLoginDTO UserLoginDTO { get; }
    }
}