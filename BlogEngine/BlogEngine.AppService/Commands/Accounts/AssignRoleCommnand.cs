﻿using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.AppService.Commands
{
    public class AssignRoleCommnand : CommandBase<ReturnUserResponse>
    {
        public AssignRoleCommnand(UserRoleDTO userRoleDTO)
        {
            UserRoleDTO = userRoleDTO;
        }

        public UserRoleDTO UserRoleDTO { get; }
    }
}