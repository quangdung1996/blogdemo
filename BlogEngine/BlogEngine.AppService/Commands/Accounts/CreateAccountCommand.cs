﻿using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.AppService.Commands
{
    public class CreateAccountCommand : CommandBase<ReturnResponse<UserTokenDTO>>
    {
        public CreateAccountCommand(UserRegisterDTO createAccount)
        {
            CreateAccount = createAccount;
        }

        internal UserRegisterDTO CreateAccount { get; }
    }
}