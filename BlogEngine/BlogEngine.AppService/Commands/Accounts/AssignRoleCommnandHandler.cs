﻿using BlogEngine.AppService.Common.Commands;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Models;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Commands
{
    internal sealed class AssignRoleCommnandHandler : ICommandHandler<AssignRoleCommnand, ReturnUserResponse>
    {
        private readonly IRoleManager _roleManager;

        public AssignRoleCommnandHandler(IRoleManager roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task<ReturnUserResponse> Handle(AssignRoleCommnand request, CancellationToken cancellationToken)
        {
            var assignmentResult = await _roleManager.AssignRoleAsync(request.UserRoleDTO);

            return new ReturnUserResponse
            {
                Errors = assignmentResult.Errors,
                Succeeded = assignmentResult.Successed,
                UserNotFound = assignmentResult.UserNotFound
            };
        }
    }
}