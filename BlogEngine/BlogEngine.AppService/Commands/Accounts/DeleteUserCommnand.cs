﻿using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models;

namespace BlogEngine.AppService.Commands
{
    public class DeleteUserCommnand : CommandBase<ReturnUserResponse>
    {
        public DeleteUserCommnand(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}