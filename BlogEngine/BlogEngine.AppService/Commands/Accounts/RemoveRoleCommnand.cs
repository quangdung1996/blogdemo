﻿using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.AppService.Commands
{
    public class RemoveRoleCommnand : AssignRoleCommnand
    {
        public RemoveRoleCommnand(UserRoleDTO userRoleDTO) : base(userRoleDTO)
        {
        }
    }
}