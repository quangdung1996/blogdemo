﻿using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.AppService.Commands
{
    public class UpdateUserCommnand : CommandBase<ReturnResponse<UserProfileDTO>>
    {
        public UpdateUserCommnand(string email, UserUpdateDTO userUpdateDTO)
        {
            Email = email;
            UserUpdateDTO = userUpdateDTO;
        }

        public string Email { get; }
        public UserUpdateDTO UserUpdateDTO { get; }
    }
}