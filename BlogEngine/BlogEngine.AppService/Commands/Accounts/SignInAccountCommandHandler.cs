﻿using BlogEngine.AppService.Common.Commands;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Commands
{
    internal sealed class SignInAccountCommandHandler : ICommandHandler<SignInAccountCommand, ReturnResponse<UserTokenDTO>>
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ITokenService _tokenService;

        public SignInAccountCommandHandler(IAuthenticationService authenticationService, ITokenService tokenService)
        {
            _authenticationService = authenticationService;
            _tokenService = tokenService;
        }

        public async Task<ReturnResponse<UserTokenDTO>> Handle(SignInAccountCommand request, CancellationToken cancellationToken)
        {
            var response = new ReturnResponse<UserTokenDTO>();
            var signInResult = await _authenticationService.LoginAsync(request.UserLoginDTO);

            if (!signInResult.Succeeded)
            {
                response.Errors = "Invalid Login attempt";
                response.Succeeded = false;
                return response;
            }

            response.Data = await _tokenService.BuildToken(request.UserLoginDTO);
            return response;
        }
    }
}