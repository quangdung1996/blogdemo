﻿using BlogEngine.AppService.Common.Commands;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Commands.Accounts
{
    internal sealed class UpdateUserCommnandHandler : ICommandHandler<UpdateUserCommnand, ReturnResponse<UserProfileDTO>>
    {
        private readonly IAccountService _service;

        public UpdateUserCommnandHandler(IAccountService service)
        {
            _service = service;
        }

        public async Task<ReturnResponse<UserProfileDTO>> Handle(UpdateUserCommnand request, CancellationToken cancellationToken)
        {
            return new ReturnResponse<UserProfileDTO>
            {
                Data = await _service.UpdateUserAsync(request.Email, request.UserUpdateDTO)
            };
        }
    }
}