﻿using BlogEngine.AppService.Common.Commands;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Models;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Commands.Accounts
{
    internal sealed class DeleteUserCommnandHandler : ICommandHandler<DeleteUserCommnand, ReturnUserResponse>
    {
        private readonly IAccountService _service;

        public DeleteUserCommnandHandler(IAccountService service)
        {
            _service = service;
        }

        public async Task<ReturnUserResponse> Handle(DeleteUserCommnand request, CancellationToken cancellationToken)
        {
            return await _service.DeleteAsync(request.Id);
        }
    }
}