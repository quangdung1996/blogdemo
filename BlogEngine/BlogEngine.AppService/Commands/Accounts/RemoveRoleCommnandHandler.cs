﻿using BlogEngine.AppService.Common.Commands;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Models;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Commands
{
    internal sealed class RemoveRoleCommnandHandler : ICommandHandler<RemoveRoleCommnand, ReturnUserResponse>
    {
        private readonly IRoleManager _roleManager;

        public RemoveRoleCommnandHandler(IRoleManager roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task<ReturnUserResponse> Handle(RemoveRoleCommnand request, CancellationToken cancellationToken)
        {
            var removeResult = await _roleManager.RemoveRoleAsync(request.UserRoleDTO);

            return new ReturnUserResponse
            {
                Errors = removeResult.Errors,
                Succeeded = removeResult.Successed,
                UserNotFound = removeResult.UserNotFound
            };
        }
    }
}