﻿using BlogEngine.AppService.Common.Commands;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Commands
{
    internal sealed class CreateAccountCommandHandler : ICommandHandler<CreateAccountCommand, ReturnResponse<UserTokenDTO>>
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly ITokenService _tokenService;

        public CreateAccountCommandHandler(IAuthenticationService authenticationService, ITokenService tokenService)
        {
            _authenticationService = authenticationService;
            _tokenService = tokenService;
        }

        public async Task<ReturnResponse<UserTokenDTO>> Handle(CreateAccountCommand request, CancellationToken cancellationToken)
        {
            var response = new ReturnResponse<UserTokenDTO>();
            var identityResult = await _authenticationService.RegisterAsync(request.CreateAccount);

            if (!identityResult.Succeeded)
            {
                response.Errors = identityResult.Errors.ToString();
                response.Succeeded = false;
                return response;
            }
            response.Data = await _tokenService.BuildToken(request.CreateAccount);
            return response;
        }
    }
}