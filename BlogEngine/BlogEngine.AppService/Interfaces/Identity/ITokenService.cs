﻿using BlogEngine.Shared.Models.DTOs;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Interfaces
{
    public interface ITokenService
    {
        Task<UserTokenDTO> BuildToken(UserInfoDTO userInfoDTO);
    }
}