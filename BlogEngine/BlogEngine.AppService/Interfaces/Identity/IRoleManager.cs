﻿using BlogEngine.Core.Models;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Interfaces
{
    public interface IRoleManager
    {
        Task<AccountOperationResult> AssignRoleAsync(UserRoleDTO userRoleDTO);

        Task<AccountOperationResult> RemoveRoleAsync(UserRoleDTO userRoleDTO);

        Task<List<string>> GetUserRolesAsync(int id);

        Task<List<string>> GetUserRolesAsync(ApplicationUser applicationUser);
    }
}