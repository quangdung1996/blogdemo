﻿using BlogEngine.Shared.Models.DTOs;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Interfaces
{
    public interface IAuthenticationService
    {
        Task<IdentityResult> RegisterAsync(UserRegisterDTO userRegisterDTO);

        Task<SignInResult> LoginAsync(UserLoginDTO userLoginDTO);
    }
}