﻿using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Interfaces
{
    public interface IAccountService
    {
        Task<UserProfileDTO> GetUserProfileDTOAsync(int id);

        Task<UserProfileDTO> GetUserProfileDTOAsync(string email);

        Task<UserInfoDetailDTO> GetUserInfoDetailDTOAsync(int id);

        Task<IEnumerable<UserInfoDetailDTO>> GetUserInfoDetailDTOsAsync();

        Task<UserProfileDTO> UpdateUserAsync(string email, UserUpdateDTO userUpdateDTO);

        Task<ReturnUserResponse> DeleteAsync(int id);
    }
}