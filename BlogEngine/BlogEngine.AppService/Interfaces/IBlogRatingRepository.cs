﻿using BlogEngine.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Interfaces
{
    public interface IBlogRatingRepository : IAsyncRepository<BlogRating>
    {
        Task<IEnumerable<BlogRating>> GetAllWithReferences();

        Task<IEnumerable<BlogRating>> GetAllByBlogIdAsync(int id);

        Task<BlogRating> GetByBlogIdAndUserIdAsync(int blogId, int userId);

        new Task<bool> InsertAsync(BlogRating blogRating);

        new Task<bool> UpdateAsync(BlogRating blogRating);
    }
}