﻿using BlogEngine.Shared;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Interfaces
{
    public interface IPageService
    {
        Task<IndexPageDTO> GetIndexPageDTOAsync();
    }
}