﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Interfaces
{
    public interface IPolicyRetry
    {
        Task<T> GetAsync<T>(Func<Task<T>> action) where T : class;

        Task<IEnumerable<T>> GetAllAsync<T>(Func<Task<IEnumerable<T>>> action) where T : class;
    }
}