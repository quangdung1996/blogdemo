﻿using BlogEngine.Shared.Models.DTOs;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Interfaces
{
    public interface IBlogRatingService
    {
        Task<bool> InsertAsync(BlogRatingDTO blogRatingDTO);
    }
}