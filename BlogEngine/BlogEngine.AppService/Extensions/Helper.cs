﻿using Dapper;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace BlogEngine.AppService.Extensions
{
    public static class Helper
    {
        private static string GetDescriptionFromAttribute(MemberInfo member)
        {
            if (member == null) return null;

            var attrib = (DescriptionAttribute)Attribute.GetCustomAttribute(member, typeof(DescriptionAttribute), false);
            return (attrib?.Description ?? member.Name).ToLower();
        }

        public static void SetTypeMap<T>() where T : class
        {
            var map = new CustomPropertyTypeMap(typeof(T), (type, columnName) => type.GetProperties().FirstOrDefault(prop => Helper.GetDescriptionFromAttribute(prop) == columnName.ToLower()));
            Dapper.SqlMapper.SetTypeMap(typeof(T), map);
        }
    }
}