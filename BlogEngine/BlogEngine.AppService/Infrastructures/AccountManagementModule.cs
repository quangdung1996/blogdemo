﻿using Autofac;
using BlogEngine.AppService.Common;
using BlogEngine.AppService.Common.Processing;
using BlogEngine.Shared.Contracts;
using MediatR;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Infrastructures
{
    internal sealed class AccountManagementModule : IAccountManagementModule
    {
        public async Task<TResult> ExecuteCommandAsync<TResult>(ICommand<TResult> command)
        {
            return await CommandsExecutor.ExecuteAsync(command);
        }

        public async Task ExecuteCommandAsync(ICommand command)
        {
            await CommandsExecutor.ExecuteAsync(command);
        }

        public async Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query)
        {
            using (var scope = AdministrationCompositionRoot.BeginLifetimeScope())
            {
                var mediator = scope.Resolve<IMediator>();

                return await mediator.Send(query);
            }
        }
    }
}