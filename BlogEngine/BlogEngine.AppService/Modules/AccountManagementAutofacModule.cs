﻿using Autofac;
using BlogEngine.AppService.Infrastructures;
using BlogEngine.Shared.Contracts;

namespace BlogEngine.AppService.Modules
{
    public class AccountManagementAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AccountManagementModule>().As<IAccountManagementModule>().InstancePerLifetimeScope();

            //builder.RegisterType<TokenService>().AsSelf().As<ITokenService>().InstancePerLifetimeScope();
            //builder.RegisterType<AuthenticationService>().AsSelf().As<IAuthenticationService>().InstancePerLifetimeScope();
        }
    }
}