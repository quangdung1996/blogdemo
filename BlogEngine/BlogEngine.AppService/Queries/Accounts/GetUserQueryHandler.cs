﻿using BlogEngine.AppService.Common.Queries;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Queries
{
    internal sealed class GetUserQueryHandler : IQueryHandler<GetUserQuery, ReturnResponse<IEnumerable<UserInfoDetailDTO>>>
    {
        private readonly IAccountService _accountService;

        public GetUserQueryHandler(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public async Task<ReturnResponse<IEnumerable<UserInfoDetailDTO>>> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            var response = new ReturnResponse<IEnumerable<UserInfoDetailDTO>>();
            response.Data = await _accountService.GetUserInfoDetailDTOsAsync();
            return response;
        }
    }
}