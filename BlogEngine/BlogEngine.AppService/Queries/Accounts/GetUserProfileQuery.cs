﻿using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.AppService.Queries
{
    public class GetUserProfileQuery : QueryBase<ReturnResponse<UserProfileDTO>>
    {
        public GetUserProfileQuery(int id)
        {
            Id = id;
        }

        public GetUserProfileQuery(string email)
        {
            Email = email;
        }

        public new int Id { get; set; }
        public string Email { get; set; }
    }
}