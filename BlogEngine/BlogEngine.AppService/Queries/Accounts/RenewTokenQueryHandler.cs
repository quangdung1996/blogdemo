﻿using BlogEngine.AppService.Common.Queries;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Models.DTOs;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Queries
{
    internal sealed class RenewTokenQueryHandler : IQueryHandler<RenewTokenQuery, UserTokenDTO>
    {
        private readonly ITokenService _tokenService;

        public RenewTokenQueryHandler(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        public async Task<UserTokenDTO> Handle(RenewTokenQuery request, CancellationToken cancellationToken)
        {
            return await _tokenService.BuildToken(request.UserInfoDTO);
        }
    }
}