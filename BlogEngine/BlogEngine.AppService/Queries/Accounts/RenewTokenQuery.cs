﻿using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.AppService.Queries
{
    public class RenewTokenQuery : QueryBase<UserTokenDTO>
    {
        public RenewTokenQuery(UserInfoDTO userInfoDTO)
        {
            UserInfoDTO = userInfoDTO;
        }

        public UserInfoDTO UserInfoDTO { get; }
    }
}