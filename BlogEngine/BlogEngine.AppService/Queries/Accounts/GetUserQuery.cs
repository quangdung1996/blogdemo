﻿using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;
using System.Collections.Generic;

namespace BlogEngine.AppService.Queries
{
    public class GetUserQuery : QueryBase<ReturnResponse<IEnumerable<UserInfoDetailDTO>>>
    {
        public GetUserQuery()
        {
        }
    }
}