﻿using BlogEngine.AppService.Common.Queries;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;
using System.Threading;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Queries
{
    internal sealed class GetUserProfileQueryHandler : IQueryHandler<GetUserProfileQuery, ReturnResponse<UserProfileDTO>>
    {
        private readonly IAccountService _accountService;

        public GetUserProfileQueryHandler(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public async Task<ReturnResponse<UserProfileDTO>> Handle(GetUserProfileQuery request, CancellationToken cancellationToken)
        {
            UserProfileDTO result = null;
            if (!string.IsNullOrEmpty(request.Email))
            {
                result = await _accountService.GetUserProfileDTOAsync(request.Email);
            }
            else
            {
                result = await _accountService.GetUserProfileDTOAsync(request.Id);
            }

            return new ReturnResponse<UserProfileDTO>
            {
                Data = result,
                Succeeded = result != null
            };
        }
    }
}