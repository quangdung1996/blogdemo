﻿using Autofac;
using BlogEngine.AppService.Common;
using BlogEngine.AppService.Modules;
using System.Collections.Generic;

namespace BlogEngine.AppService
{
    public static class AppServiceStartup
    {
        private static IContainer _container;

        public static IContainer Initialize(ContainerBuilder containerBuilder)
        {
            return ConfigureContainer(containerBuilder);
        }

        private static IContainer ConfigureContainer(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterModule(new AccountManagementAutofacModule());
            containerBuilder.RegisterModule(new MediatorModule());
            //containerBuilder.RegisterInstance(_container).As<Autofac.IContainer>();
            //containerBuilder.RegisterModule(new List<Module)
            _container = containerBuilder.Build();
            AdministrationCompositionRoot.SetContainer(_container);
            return _container;
        }

        public static ContainerBuilder RegisterModule<T>(this ContainerBuilder containerBuilder, List<T> modules) where T : Module
        {
            foreach (var module in modules)
            {
                containerBuilder.RegisterModule(module);
            }
            return containerBuilder;
        }
    }
}