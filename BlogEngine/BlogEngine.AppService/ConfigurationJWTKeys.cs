﻿using BlogEngine.AppService;
using BlogEngine.AppService.Services;
using BlogEngine.Shared.Validations;

[assembly: ConfigurationKey(nameof(TokenService), ConfigurationJWTKeys.JWtKey)]

namespace BlogEngine.AppService
{
    internal static class ConfigurationJWTKeys
    {
        internal const string JWtKey = "JWT:Key";
        internal const string ValidIssuerKey = "JWT:ValidIssuerKey";
        internal const string ValidAudienceKey = "JWT:ValidAudienceKey";
        internal const string IssuerSigningKey = "JWT:IssuerSigningKey";
    }
}