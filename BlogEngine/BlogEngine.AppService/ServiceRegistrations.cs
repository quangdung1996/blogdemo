﻿using AutoMapper;
using BlogEngine.AppService.Infrastructures;
using BlogEngine.AppService.Interfaces;
using BlogEngine.AppService.Services;
using BlogEngine.Core;
using BlogEngine.Core.Models;
using BlogEngine.Core.Services;
using BlogEngine.Shared.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Reflection;
using System.Text;

namespace BlogEngine.AppService
{
    public static class ServiceRegistrations
    {
        public static IServiceCollection AddAuthencation(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddIdentity();
            services.AddDataAcess(configuration);
            services.AddJWTAuthentication(configuration);
            services.AddScoped<IAccountManagementModule, AccountManagementModule>();
            services.AddMapper();

            services.AddUserProviders();

            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IRoleManager, RoleManager>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IPolicyRetry, PolicyRetry>();
            services.AddScoped<IPageService, PageService>();
            services.AddScoped<IBlogRatingService, BlogRatingService>();

            services.TryAddScoped<ISqlConnectionFactory>(cfg =>
            {
                return new SqlConnectionFactory(configuration.GetConnectionString("DbConnection"));
            });

            services.TryAddScoped(typeof(IAsyncRepository<>), typeof(AsyncRepository<>));
            services.TryAddScoped<IBlogRatingRepository, BlogRatingRepository>();

            return services.AddScoped<ITokenService, TokenService>();
        }

        private static void AddIdentity(this IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, IdentityRole<int>>(config =>
            {
                // Temporary simple validation
                config.Password.RequiredLength = 1;
                config.Password.RequireDigit = false;
                config.Password.RequireLowercase = false;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireUppercase = false;
            }).AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();
        }

        public static void AddDataAcess(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DbConnection"),
                    b => b.MigrationsAssembly(Assembly.GetAssembly(typeof(ApplicationDbContext)).FullName));
            });
        }

        private static void AddMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(configuration =>
            {
                configuration.AllowNullCollections = true;
                configuration.AllowNullDestinationValues = true;
            }, typeof(ServiceRegistrations));
        }

        private static void AddJWTAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,

                        ValidIssuer = configuration[ConfigurationJWTKeys.ValidIssuerKey],
                        ValidAudience = configuration[ConfigurationJWTKeys.ValidAudienceKey],
                        IssuerSigningKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration[ConfigurationJWTKeys.IssuerSigningKey])),
                        TokenDecryptionKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration[ConfigurationJWTKeys.JWtKey])),
                        ClockSkew = TimeSpan.FromMinutes(0),
                    };
                });
        }

        private static void AddUserProviders(this IServiceCollection services)
        {
            services.AddScoped<ICurrentUserProvider, CurrentUserProvider>();
        }
    }
}