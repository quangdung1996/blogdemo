﻿using BlogEngine.AppService.Interfaces;
using BlogEngine.Core.Models;
using BlogEngine.Shared.Extensions;
using BlogEngine.Shared.Models.DTOs;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Services
{
    internal sealed class TokenService : ITokenService
    {
        private readonly IConfiguration _configuration;
        private readonly UserManager<ApplicationUser> _userManager;

        public TokenService(IConfiguration configuration, UserManager<ApplicationUser> userManager)
        {
            _configuration = configuration;
            _userManager = userManager;
        }

        public async Task<UserTokenDTO> BuildToken(UserInfoDTO userInfoDTO)
        {
            Preconditions.NotNull(userInfoDTO, nameof(userInfoDTO));

            var identityUser = await _userManager.FindByEmailAsync(userInfoDTO.EmailAddress);

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, identityUser.FirstName),
                new Claim(ClaimTypes.Surname, identityUser.LastName),
                new Claim(ClaimTypes.Email, userInfoDTO.EmailAddress),
            };

            var userClaims = await _userManager.GetClaimsAsync(identityUser);

            claims.AddRange(userClaims);

            var credentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration[ConfigurationJWTKeys.IssuerSigningKey])), SecurityAlgorithms.HmacSha256);

            var encryptingCredentials = new EncryptingCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration[ConfigurationJWTKeys.JWtKey])),
                                                                  JwtConstants.DirectKeyUseAlg,
                                                                  SecurityAlgorithms.Aes256CbcHmacSha512);

            var expirationDate = DateTime.UtcNow.AddMinutes(5);// DateTime.Now.AddDays(7);

            JwtSecurityToken JWTToken = new JwtSecurityTokenHandler().CreateJwtSecurityToken(
                    issuer: _configuration[ConfigurationJWTKeys.ValidIssuerKey],
                    audience: _configuration[ConfigurationJWTKeys.ValidAudienceKey],
                    subject: new ClaimsIdentity(claims),
                    null,
                    expires: expirationDate,
                    null,
                    signingCredentials: credentials,
                    encryptingCredentials: encryptingCredentials);

            var token = new JwtSecurityTokenHandler().WriteToken(JWTToken);

            return new UserTokenDTO(token, expirationDate);
        }
    }
}