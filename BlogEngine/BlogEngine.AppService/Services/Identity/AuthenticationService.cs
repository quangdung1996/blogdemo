﻿using AutoMapper;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Core.Models;
using BlogEngine.Shared.Extensions;
using BlogEngine.Shared.Models.DTOs;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Services
{
    internal sealed class AuthenticationService : IAuthenticationService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IMapper _mapper;

        public AuthenticationService(UserManager<ApplicationUser> userManager,
                                     SignInManager<ApplicationUser> signInManager,
                                     IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
        }

        public async Task<SignInResult> LoginAsync(UserLoginDTO userLoginDTO)
        {
            Preconditions.NotNull(userLoginDTO, nameof(userLoginDTO));
            return await _signInManager.PasswordSignInAsync(userLoginDTO.EmailAddress, userLoginDTO.Password, false, false);
        }

        public async Task<IdentityResult> RegisterAsync(UserRegisterDTO userRegisterDTO)
        {
            Preconditions.NotNull(userRegisterDTO, nameof(userRegisterDTO));
            var applicationUser = _mapper.Map<ApplicationUser>(userRegisterDTO);
            return await _userManager.CreateAsync(applicationUser, userRegisterDTO.Password);
        }
    }
}