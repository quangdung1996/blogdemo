﻿using AutoMapper;
using BlogEngine.AppService.Extensions;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Core.Exceptions;
using BlogEngine.Core.Models;
using BlogEngine.Core.Services;
using BlogEngine.Shared.Extensions;
using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;
using Dapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Services
{
    internal class AccountService : IAccountService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ICurrentUserProvider _currentUserProvider;
        private readonly IRoleManager _roleManager;
        private readonly IMapper _mapper;
        private readonly ISqlConnectionFactory _sqlConnectionFactory;
        private readonly IPolicyRetry _policyRetry;

        public AccountService(UserManager<ApplicationUser> userManager,
                              ICurrentUserProvider currentUserProvider,
                              IRoleManager roleManager,
                              IMapper mapper,
                              ISqlConnectionFactory sqlConnectionFactory,
                              IPolicyRetry policyRetry)
        {
            _userManager = userManager;
            _currentUserProvider = currentUserProvider;
            _roleManager = roleManager;
            _mapper = mapper;
            _sqlConnectionFactory = sqlConnectionFactory;
            _policyRetry = policyRetry;
        }

        public async Task<ReturnUserResponse> DeleteAsync(int id)
        {
            const string sql = "SELECT * FROM [dbo].[AspNetUsers]";
            var connection = _sqlConnectionFactory.GetOpenConnection();
            var user = await _policyRetry.GetAsync(async () =>
            {
                return await connection.QueryFirstOrDefaultAsync<ApplicationUser>(sql);
            });

            if (user is null)
            {
                return new ReturnUserResponse()
                {
                    UserNotFound = true,
                    Errors = $"User with a ID = '{id}' was not found in the Database",
                    Succeeded = false
                };
            }

            var identityResult = await _userManager.DeleteAsync(user);

            return new ReturnUserResponse()
            {
                Succeeded = identityResult.Succeeded,
                Errors = string.Join(", ", identityResult.Errors)
            };
        }

        public Task<UserInfoDetailDTO> GetUserInfoDetailDTOAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IEnumerable<UserInfoDetailDTO>> GetUserInfoDetailDTOsAsync()
        {
            var connection = _sqlConnectionFactory.GetOpenConnection();
            Helper.SetTypeMap<UserInfoDetailDTO>();

            const string sql = "SELECT * FROM [dbo].[AspNetUsers]";
            var applicationUsers = await _policyRetry.GetAllAsync(async () =>
            {
                return await connection.QueryAsync<UserInfoDetailDTO>(sql);
            });

            foreach (var applicationUser in applicationUsers)
            {
                applicationUser.Roles = await _roleManager.GetUserRolesAsync(applicationUser.ID);
            }
            return applicationUsers;
        }

        public async Task<UserProfileDTO> GetUserProfileDTOAsync(int id)
        {
            var connection = _sqlConnectionFactory.GetOpenConnection();
            Helper.SetTypeMap<UserProfileDTO>();
            var dynamicParameters = new DynamicParameters();
            const string sql = "SELECT * FROM [dbo].[AspNetUsers] WHERE Id=@id";
            dynamicParameters.Add("@id", id, DbType.Int32);
            var applicationUser = await _policyRetry.GetAsync(async () =>
            {
                return await connection.QueryFirstAsync<UserProfileDTO>(sql, dynamicParameters);
            });

            if (applicationUser is null) return null;
            applicationUser.Roles = await _roleManager.GetUserRolesAsync(applicationUser.ID);

            return applicationUser;
        }

        public async Task<UserProfileDTO> GetUserProfileDTOAsync(string email)
        {
            var connection = _sqlConnectionFactory.GetOpenConnection();
            Helper.SetTypeMap<UserInfoDetailDTO>();
            var dynamicParameters = new DynamicParameters();
            const string sql = "SELECT * FROM [dbo].[AspNetUsers] WHERE Email=@email";
            dynamicParameters.Add("@email", email, DbType.String);
            var applicationUser = await _policyRetry.GetAsync(async () =>
            {
                return await connection.QueryFirstAsync<UserProfileDTO>(sql, dynamicParameters);
            });

            if (applicationUser is null) return null;
            applicationUser.Roles = await _roleManager.GetUserRolesAsync(applicationUser.ID);

            return applicationUser;
        }

        public async Task<UserProfileDTO> UpdateUserAsync(string email, UserUpdateDTO userUpdateDTO)
        {
            Preconditions.NotNullOrWhiteSpace(email, nameof(email));
            Preconditions.NotNull(userUpdateDTO, nameof(userUpdateDTO));

            var applicationUser = await _userManager.FindByEmailAsync(email);

            if (applicationUser is null) throw new EntityNotFoundException(nameof(ApplicationUser));

            var currentUser = await _currentUserProvider.GetCurrentUserAsync();

            if (currentUser is null) throw new UnauthorizedAccessException();

            if (!applicationUser.Equals(currentUser)) throw new InvalidOperationException();

            applicationUser.FirstName = userUpdateDTO.FirstName;
            applicationUser.LastName = userUpdateDTO.LastName;
            applicationUser.ProfilePicture = userUpdateDTO.ProfilePicture;

            var identityResult = await _userManager.UpdateAsync(applicationUser);

            if (!identityResult.Succeeded) return null;

            var userProfileDTO = _mapper.Map<UserProfileDTO>(applicationUser);

            userProfileDTO.Roles = await _roleManager.GetUserRolesAsync(applicationUser);

            return userProfileDTO;
        }

        protected async Task<UserInfoDetailDTO> MapWithRolesAsync(UserInfoDetailDTO applicationUser)
        {
            applicationUser.Roles = await _roleManager.GetUserRolesAsync(applicationUser.ID);
            return applicationUser;
        }
    }
}