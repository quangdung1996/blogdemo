﻿using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Services
{
    internal sealed class PageService : IPageService
    {
        public PageService()
        {
        }

        public async Task<IndexPageDTO> GetIndexPageDTOAsync()
        {
            return new IndexPageDTO
            {
                FeaturedCategoryDTOs = new List<Shared.Models.DTOs.CategoryDTO>
                {
                    new Shared.Models.DTOs.CategoryDTO
                    {
                        ID=1,
                        Name="123"
                    }
                },
                NewBlogDTOs = new List<Shared.Models.DTOs.BlogDTO>
                {
                    new Shared.Models.DTOs.BlogDTO
                    {
                        ID=2,
                        ShortDescription="12"
                    }
                }
            };
        }
    }
}