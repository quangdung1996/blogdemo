﻿using BlogEngine.AppService.Interfaces;
using BlogEngine.Core;
using BlogEngine.Core.Models;
using BlogEngine.Shared.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Services
{
    internal class BlogRatingRepository : AsyncRepository<BlogRating>, IBlogRatingRepository
    {
        private readonly ApplicationDbContext _context;

        public BlogRatingRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public Task<IEnumerable<BlogRating>> GetAllByBlogIdAsync(int id) => Task.FromResult(_context.BlogRatings
              .Where(br => br.BlogID.Equals(id))
              .Include(br => br.Blog)
              .AsEnumerable());

        public Task<IEnumerable<BlogRating>> GetAllWithReferences()
        {
            return (Task<IEnumerable<BlogRating>>)_context.BlogRatings
                .Include(br => br.Blog)
                .AsEnumerable();
        }

        public async Task<BlogRating> GetByBlogIdAndUserIdAsync(int blogId, int userId)
        {
            return await _context.BlogRatings
           .Where(br => br.BlogID.Equals(blogId) && br.ApplicationUserID.Equals(userId))
           .Include(br => br.Blog)
           .FirstOrDefaultAsync();
        }

        public override async Task<BlogRating> GetByIdAsync(int id)
        {
            return await _context.BlogRatings
                .Where(br => br.ID.Equals(id))
                .Include(br => br.Blog)
                .FirstOrDefaultAsync();
        }

        public new async Task<bool> InsertAsync(BlogRating blogRating)
        {
            Preconditions.NotNull(blogRating, typeof(BlogRating).Name);

            await _context.BlogRatings.AddAsync(blogRating);
            await SaveChangesAsync();

            return await SaveChangesAsync();
        }

        public new async Task<bool> UpdateAsync(BlogRating blogRating)
        {
            Preconditions.NotNull(blogRating, typeof(BlogRating).Name);
            _context.BlogRatings.Update(blogRating);
            return await SaveChangesAsync();
        }
    }
}