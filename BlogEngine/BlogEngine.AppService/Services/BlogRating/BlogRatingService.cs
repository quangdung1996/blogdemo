﻿using AutoMapper;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Core.Models;
using BlogEngine.Core.Services;
using BlogEngine.Shared.Extensions;
using BlogEngine.Shared.Models.DTOs;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Services
{
    internal sealed class BlogRatingService : IBlogRatingService
    {
        private readonly IBlogRatingRepository _blogRatingRepository;
        private readonly ICurrentUserProvider _currentUserProvider;
        private readonly IMapper _mapper;

        public BlogRatingService()
        {
        }

        public async Task<bool> InsertAsync(BlogRatingDTO blogRatingDTO)
        {
            Preconditions.NotNull(blogRatingDTO, nameof(blogRatingDTO));

            var user = await _currentUserProvider.GetCurrentUserAsync();

            var currentRating = await _blogRatingRepository.GetByBlogIdAndUserIdAsync(blogRatingDTO.BlogID, user.Id);

            if (currentRating is null)
            {
                var blogRatingEntity = _mapper.Map<BlogRating>(blogRatingDTO);
                blogRatingEntity.ApplicationUserID = user.Id;
                blogRatingEntity.CreatedBy = user.FullName;
                blogRatingEntity.LastUpdateBy = user.FullName;
                return await _blogRatingRepository.InsertAsync(blogRatingEntity);
            }
            else
            {
                currentRating.Rate = blogRatingDTO.Rate;
                currentRating.LastUpdateBy = user.FullName;
                return await _blogRatingRepository.UpdateAsync(currentRating);
            }
        }
    }
}