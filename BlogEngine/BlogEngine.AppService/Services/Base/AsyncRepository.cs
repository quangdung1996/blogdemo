﻿using BlogEngine.AppService.Interfaces;
using BlogEngine.Core.Exceptions;
using BlogEngine.Core.Models;
using BlogEngine.Shared.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Services
{
    internal class AsyncRepository<TEntity> : IAsyncRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public AsyncRepository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public virtual async Task<bool> DeleteAsync(int id)
        {
            TEntity entityFromDb = await FindAsync(id);

            NullCheckThrowNotFoundException(entityFromDb);

            _dbSet.Remove(entityFromDb);
            return await SaveChangesAsync();
        }

        public virtual Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return Task.FromResult(_dbSet.AsEnumerable());
        }

        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
            return await FindAsync(id);
        }

        public virtual Task<IEnumerable<TEntity>> GetByRawSqlAsync(string query, params object[] parameters)
        {
            Preconditions.NotNullOrWhiteSpace(query, nameof(query));

            return Task.FromResult(_dbSet.FromSqlRaw(query, parameters).AsEnumerable());
        }

        public virtual async Task<TEntity> InsertAsync(TEntity entity)
        {
            Preconditions.NotNull(entity, typeof(TEntity).Name);

            await _dbSet.AddAsync(entity);
            await SaveChangesAsync();

            return entity;
        }

      
        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            Preconditions.NotNull(entity, typeof(TEntity).Name);

            _dbSet.Update(entity);
            await SaveChangesAsync();

            return entity;
        }

        protected void NullCheckThrowNotFoundException(TEntity entity)
        {
            if (entity is null)
            {
                throw new EntityNotFoundException(typeof(TEntity).Name);
            }
        }

        protected async Task<TEntity> FindAsync(int id) => await _dbSet.FindAsync(id);

        protected async Task<bool> SaveChangesAsync()
        {
            try
            {
                return await _context.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}