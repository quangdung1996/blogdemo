﻿using BlogEngine.AppService.Interfaces;
using Microsoft.Data.SqlClient;
using Polly;
using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.AppService.Services
{
    public class PolicyRetry : IPolicyRetry
    {
        public readonly AsyncRetryPolicy _retryPolicy;
        private readonly int[] _sqlExceptions = new[] { 53, -2 };
        private const int RetryCount = 3;
        private const int WaitBetweenRetriesInSeconds = 15;

        public PolicyRetry()
        {
            _retryPolicy = Policy.Handle<SqlException>(exception => _sqlExceptions.Contains(exception.Number))
                                .WaitAndRetryAsync(retryCount: RetryCount,
                                                   sleepDurationProvider: attempt => TimeSpan.FromSeconds(WaitBetweenRetriesInSeconds));
        }

        public async Task<T> GetAsync<T>(Func<Task<T>> action) where T : class
        {
            return await _retryPolicy.ExecuteAsync(async () =>
            {
                return await action.Invoke();
            });
        }

        public async Task<IEnumerable<T>> GetAllAsync<T>(Func<Task<IEnumerable<T>>> action) where T : class
        {
            return await _retryPolicy.ExecuteAsync(async () =>
            {
                return await action.Invoke();
            });
        }
    }
}