﻿using BlogEngine.ClientServices.Interfaces;
using BlogEngine.Shared.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.ClientServices.Services
{
    internal sealed class AccountClient : IAccountClient
    {
        private readonly IHttpService _httpService;

        public AccountClient(IHttpService httpService)
        {
            _httpService = httpService;
        }

        public Task<bool> AssignRoleAsync(UserRoleDTO userRoleDTO)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteUserAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<UserInfoDetailDTO>> GetUserInfoDetailDTOsAsync()
        {
            throw new NotImplementedException();
        }

        public Task<UserProfileDTO> GetUserProfileDTOAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<UserProfileDTO> GetUserProfileDTOAsync(string email)
        {
            throw new NotImplementedException();
        }

        public Task<UserTokenDTO> LoginAsync(UserLoginDTO userLoginDTO)
        {
            throw new NotImplementedException();
        }

        public Task<UserTokenDTO> RegisterAsync(UserRegisterDTO userRegisterDTO)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RemoveRoleAsync(UserRoleDTO userRoleDTO)
        {
            throw new NotImplementedException();
        }

        public Task<UserProfileDTO> UpdateUserAsync(string email, UserUpdateDTO userUpdateDTO)
        {
            throw new NotImplementedException();
        }
    }
}