﻿using BlogEngine.ClientServices.Common.Endpoints;
using BlogEngine.ClientServices.Common.Extensions;
using BlogEngine.ClientServices.Interfaces;
using BlogEngine.Shared;
using System.Threading.Tasks;

namespace BlogEngine.ClientServices.Services
{
    internal sealed class PageClient : IPageClient
    {
        private readonly IHttpService _httpService;

        public PageClient(IHttpService httpService)
        {
            _httpService = httpService;
        }

        public async Task<IndexPageDTO> GetIndexPageDTOAsync()
        {
            return await _httpService.GetHelperAsync<IndexPageDTO>(PageClientEndpoints.Index);
        }
    }
}