﻿using System.Threading.Tasks;

namespace BlogEngine.ClientServices.Interfaces
{
    public interface ILoginService
    {
        Task LoginAsync(string token);

        Task LogoutAsync();
    }
}