﻿using BlogEngine.Shared;
using System.Threading.Tasks;

namespace BlogEngine.ClientServices.Interfaces
{
    public interface IPageClient
    {
        Task<IndexPageDTO> GetIndexPageDTOAsync();
    }
}