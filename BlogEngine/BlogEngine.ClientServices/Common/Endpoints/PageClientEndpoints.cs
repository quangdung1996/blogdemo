﻿namespace BlogEngine.ClientServices.Common.Endpoints
{
    public sealed class PageClientEndpoints
    {
        public const string Base = "v1/api/pages";
        public const string Index = "v1/api/pages/index";
    }
}