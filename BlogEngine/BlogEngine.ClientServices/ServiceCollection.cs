﻿using BlogEngine.ClientServices.Account;
using BlogEngine.ClientServices.Interfaces;
using BlogEngine.ClientServices.Services;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;

namespace BlogEngine.ClientServices
{
    public static class ServiceCollection
    {
        public static IServiceCollection AddClientServices(this IServiceCollection services)
        {
            services.AddHttpClientService();

            services.AddClientAPIServices();

            services.AddJSInteropServices();

            services.AddJWTAuthenticationServices();

            return services;
        }

        private static void AddHttpClientService(this IServiceCollection services)
        {
            // TODO: HttpClient BaseAdress should parsed from  ***.JSON file
            services.AddScoped<HttpClient>(s =>
            {
                return new HttpClient { BaseAddress = new Uri("https://localhost:5005") };
                //  return new HttpClient { BaseAddress = new Uri("https://apiserver1234.herokuapp.com") };
            });
        }

        private static void AddClientAPIServices(this IServiceCollection services)
        {
            services.AddScoped<IHttpService, HttpService>();
            services.AddScoped<IPageClient, PageClient>();
            services.AddScoped<IAccountClient, AccountClient>();
        }

        private static void AddJSInteropServices(this IServiceCollection services)
        {
            services.AddScoped<IBrowserStorageService, BrowserStorageService>();
        }

        private static void AddJWTAuthenticationServices(this IServiceCollection services)
        {
            services.AddScoped<JWTAuthenticationStateProvider>();

            services.AddScoped<IJWTClaimParserService, JWTClaimParserService>();

            services.AddScoped<AuthenticationStateProvider, JWTAuthenticationStateProvider>(
                provider => provider.GetRequiredService<JWTAuthenticationStateProvider>());

            services.AddScoped<ILoginService, JWTAuthenticationStateProvider>(
                provider => provider.GetRequiredService<JWTAuthenticationStateProvider>());

            services.AddOptions();
            services.AddAuthorizationCore(opt => { });
        }
    }
}