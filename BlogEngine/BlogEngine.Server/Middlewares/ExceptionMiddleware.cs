﻿using BlogEngine.Server.Extensions;
using BlogEngine.Shared.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace BlogEngine.Server.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (HttpStatusCodeException ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
            catch (Exception exceptionObj)
            {
                await HandleExceptionAsync(httpContext, exceptionObj);
            }
        }

        private async Task HandleExceptionAsync(HttpContext httpContext, Exception ex)
        {
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            var errorDetails = new ErrorDetails(httpContext.Response.StatusCode, $"Internal Server Error: { ex.Message }");
            string errorDetailsJson = JsonConvert.SerializeObject(errorDetails);
            await httpContext.Response.WriteAsync(errorDetailsJson);
        }

        private Task HandleExceptionAsync(HttpContext context, HttpStatusCodeException exception)
        {
            string result = null;
            context.Response.ContentType = "application/json";
            if (exception is HttpStatusCodeException)
            {
                result = new ErrorDetails((int)exception.StatusCode, exception.Message
                ).ToString();
                context.Response.StatusCode = (int)exception.StatusCode;
            }
            else
            {
                result = new ErrorDetails((int)HttpStatusCode.BadRequest, "Runtime Error").ToString();
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            return context.Response.WriteAsync(result);
        }
    }
}