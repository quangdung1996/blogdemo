﻿using BlogEngine.AppService.Commands;
using BlogEngine.AppService.Interfaces;
using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Models.DTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BlogEngine.Server.Controllers.V1
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("/v{version:apiVersion}/api/[controller]")]
    public class BlogRatingsController : BaseController
    {
        private readonly IBlogRatingService _blogratingService;
        private readonly IAccountManagementModule _accountManagementModule;

        public BlogRatingsController(IBlogRatingService blogratingservice, IAccountManagementModule accountManagementModule)
        {
            _blogratingService = blogratingservice;
            _accountManagementModule = accountManagementModule;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> Post([FromBody] BlogRatingDTO blogratingdto)
        {
            var result = await _accountManagementModule.ExecuteCommandAsync(new CreateBlogRatingCommnand(blogratingdto));
            if (!result.Succeeded) return BadRequest();
            return NoContent();
        }
    }
}