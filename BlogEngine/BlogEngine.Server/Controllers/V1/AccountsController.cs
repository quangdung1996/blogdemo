﻿using BlogEngine.AppService.Commands;
using BlogEngine.AppService.Queries;
using BlogEngine.Shared.Contracts;
using BlogEngine.Shared.Helpers;
using BlogEngine.Shared.Models.DTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogEngine.Server.Controllers.V1
{
    public class AccountsController : BaseController
    {
        private readonly IAccountManagementModule _accountManagementModule;

        public AccountsController(IAccountManagementModule managementModule)
        {
            _accountManagementModule = managementModule;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserRegisterDTO userRegisterDTO)
        {
            var result = await _accountManagementModule.ExecuteCommandAsync(new CreateAccountCommand(userRegisterDTO));
            if (!result.Succeeded) return BadRequest(result.Errors);
            return Ok(result);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] UserLoginDTO userLoginDTO)
        {
            var result = await _accountManagementModule.ExecuteCommandAsync(new SignInAccountCommand(userLoginDTO));
            if (!result.Succeeded) return BadRequest(result.Errors);
            return Ok(result);
        }

        [HttpGet("users")]
        [ProducesResponseType(typeof(List<UserInfoDetailDTO>), StatusCodes.Status200OK)]
        public async Task<ActionResult<List<UserInfoDetailDTO>>> GetUsers()
        {
            var result = await _accountManagementModule.ExecuteQueryAsync(new GetUserQuery());
            return Ok(result);
        }

        [HttpGet("user/profile/{id:int}")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(UserProfileDTO), StatusCodes.Status200OK)]
        public async Task<ActionResult<UserProfileDTO>> GetUser(int id)
        {
            var result = await _accountManagementModule.ExecuteQueryAsync(new GetUserProfileQuery(id));
            if (!result.Succeeded) return NotFound();
            return Ok(result);
        }

        [HttpGet("user/profile/{email}")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(UserProfileDTO), StatusCodes.Status200OK)]
        public async Task<ActionResult<UserProfileDTO>> GetUser(string email)
        {
            var result = await _accountManagementModule.ExecuteQueryAsync(new GetUserProfileQuery(email));
            if (!result.Succeeded) return NotFound();
            return Ok(result);
        }

        [HttpPut("update/{email}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(typeof(UserProfileDTO), StatusCodes.Status200OK)]
        public async Task<ActionResult<UserProfileDTO>> UpdateUser(string email, [FromBody] UserUpdateDTO userUpdateDTO)
        {
            var result = await _accountManagementModule.ExecuteCommandAsync(new UpdateUserCommnand(email, userUpdateDTO));
            return Ok(result);
        }

        [HttpPost("assignRole")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = UserRole.Admin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        public async Task<ActionResult<bool>> AssignRole([FromBody] UserRoleDTO userRoleDTO)
        {
            var result = await _accountManagementModule.ExecuteCommandAsync(new AssignRoleCommnand(userRoleDTO));
            if (result.UserNotFound) return NotFound(result.Errors);
            return Ok(result);
        }

        [HttpDelete("removeRole")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = UserRole.Admin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        public async Task<ActionResult<bool>> RemoveRole([FromBody] UserRoleDTO userRoleDTO)
        {
            var result = await _accountManagementModule.ExecuteCommandAsync(new RemoveRoleCommnand(userRoleDTO));
            if (result.UserNotFound) return NotFound(result.Errors);
            return Ok(result);
        }

        [HttpDelete("{id:int}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = UserRole.Admin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        public async Task<ActionResult<bool>> DeleteUser(int id)
        {
            var result = await _accountManagementModule.ExecuteCommandAsync(new DeleteUserCommnand(id));
            if (result.UserNotFound) return NotFound(result.Errors);
            return Ok(result);
        }

        [HttpPost("renewToken")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<UserTokenDTO>> RenewToken()
        {
            var userInfo = new UserInfoDTO()
            {
                EmailAddress = HttpContext.User.Identity.Name
            };

            return Ok(await _accountManagementModule.ExecuteQueryAsync(new RenewTokenQuery(userInfo)));
        }
    }
}