﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlogEngine.Server.Controllers
{
    [ApiController]
    [Area("Hello")]
    [ApiExplorerSettings(GroupName = "'BlogEngine.Server v'VVVV")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [ApiVersion("1.0")]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [Route("/v{version:apiVersion}/api/[controller]")]
    public abstract class BaseController : ControllerBase
    {
    }
}