using Autofac;
using Autofac.Extensions.DependencyInjection;
using BlogEngine.AppService;
using BlogEngine.Server.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using QD.Swagger.Extensions;
using System;

namespace BlogEngine.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddAuthencation(Configuration);

            services.AddSwaggerForApiDocs("'BlogEngine.Server v'VVVV", options => { });
            return CreateAutofacServiceProvider(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.ConfigureExceptionMiddleware();
            app.ConfigureLoggingMiddleware();
            app.UseRouting();

            app.UseAuthorization();
            app.UseSwaggerForApiDocs("BlogEngine APIs", false);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private IServiceProvider CreateAutofacServiceProvider(IServiceCollection services)
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);

            //var container = containerBuilder.Build();
            var container = AppServiceStartup.Initialize(containerBuilder);

            return new AutofacServiceProvider(container);
        }
    }
}