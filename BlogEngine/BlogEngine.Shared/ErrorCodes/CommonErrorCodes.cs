﻿namespace BlogEngine.Shared.ErrorCodes
{
    public static class CommonErrorCodes
    {
        public const string ACCOUNT_NOT_FOUND = "Account {0} not existed or you don't have permission.";
        public const string USER_NOT_FOUND = "UserNotFound";
    }
}