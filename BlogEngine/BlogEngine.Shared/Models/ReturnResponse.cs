﻿namespace BlogEngine.Shared.Models
{
    public class ReturnResponse<T> : ReturnResponse
    {
        public T Data { get; set; }

        public ReturnResponse() : base()
        {
        }
    }

    public class ReturnResponse
    {
        public bool Succeeded { get; set; }
        public string Errors { get; set; }

        public ReturnResponse()
        {
            Succeeded = true;
        }
    }

    public class ReturnUserResponse : ReturnResponse
    {
        public bool UserNotFound { get; set; }

        public ReturnUserResponse() : base()
        {
            UserNotFound = false;
        }
    }
}