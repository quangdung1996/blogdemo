﻿namespace BlogEngine.Shared.Models.DTOs
{
    public class UserRoleDTO
    {
        public int UserID { get; set; }
        public string RoleName { get; set; }
    }
}