﻿using System.Collections.Generic;
using System.ComponentModel;

namespace BlogEngine.Shared.Models.DTOs
{
    public class UserInfoDetailDTO
    {
        public int ID { get; set; }

        [Description("Email")]
        public string EmailAddress { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public string FullName => $"{FirstName} {LastName}";

        [Description("Roles")]
        public List<string> Roles { get; set; } = new List<string>();
    }
}