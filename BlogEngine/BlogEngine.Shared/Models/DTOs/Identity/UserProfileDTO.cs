﻿using System.Collections.Generic;

namespace BlogEngine.Shared.Models.DTOs
{
    public class UserProfileDTO : UserInfoDetailDTO
    {
        public List<BlogDTO> BlogDTOs { get; set; } = new List<BlogDTO>();
    }
}