﻿namespace BlogEngine.Shared.Models.DTOs
{
    public class SubCommentDTO : CommentDTOBase
    {
        public int MainCommentID { get; set; }
    }
}