﻿using System.Collections.Generic;

namespace BlogEngine.Shared.Models.DTOs
{
    public class MainCommentDTO : CommentDTOBase
    {
        public List<SubCommentDTO> SubCommentDTOs { get; set; } = new List<SubCommentDTO>();
    }
}