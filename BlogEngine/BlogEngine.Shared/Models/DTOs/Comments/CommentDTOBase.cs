﻿namespace BlogEngine.Shared.Models.DTOs
{
    public class CommentDTOBase : ReadDataDTOBase
    {
        public int ID { get; set; }
        public int BlogID { get; set; }
        public int ApplicationUserID { get; set; }
        public UserInfoDetailDTO UserInfoDetailDTO { get; set; }
        public string Body { get; set; }
    }
}