﻿using BlogEngine.Shared.Models.DTOs;
using System.Collections.Generic;

namespace BlogEngine.Shared
{
    public class IndexPageDTO
    {
        public List<BlogDTO> NewBlogDTOs { get; set; } = new List<BlogDTO>();
        public List<CategoryDTO> FeaturedCategoryDTOs { get; set; } = new List<CategoryDTO>();
    }
}