﻿using MediatR;

namespace BlogEngine.Shared.Contracts
{
    public interface IQuery<out TResult> : IRequest<TResult>
    {
    }
}