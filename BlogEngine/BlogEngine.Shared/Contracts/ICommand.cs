﻿using MediatR;

namespace BlogEngine.Shared.Contracts
{
    public interface ICommand<out TResult> : IRequest<TResult>
    {
    }

    public interface ICommand : IRequest<Unit>
    {
    }
}