﻿using System.Threading.Tasks;

namespace BlogEngine.Shared.Contracts
{
    public interface IAccountManagementModule
    {
        Task<TResult> ExecuteCommandAsync<TResult>(ICommand<TResult> command);

        Task ExecuteCommandAsync(ICommand command);

        Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query);
    }
}