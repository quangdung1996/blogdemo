﻿using BlogEngine.Shared.Models;
using BlogEngine.Shared.Models.DTOs;

namespace BlogEngine.Shared.Contracts
{
    public abstract class CommandBase<TResult> : ICommand<TResult>
    {
        protected CommandBase()
        {
            this.ReturnResponse = new ReturnResponse<UserTokenDTO>();
        }

        protected CommandBase(ReturnResponse<UserTokenDTO> returnResponse)
        {
            ReturnResponse = returnResponse;
        }

        public ReturnResponse<UserTokenDTO> ReturnResponse { get; }
    }
}